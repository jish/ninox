# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ninox,
  ecto_repos: [Ninox.Repo]

# Configures the endpoint
config :ninox, NinoxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "nryeIQr+A2LRy/ulvGFfwukibPzqjxzvchAEz5W/8FaYLOzvbWzKzW1ZB0VpUfHn",
  render_errors: [view: NinoxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ninox.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
