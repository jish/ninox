defmodule NinoxWeb.PageController do
  use NinoxWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
