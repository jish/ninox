defmodule Ninox.LogBook.Entry do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ninox.LogBook.Entry


  schema "entries" do
    field :count, :integer
    field :occurred_at, :utc_datetime
    field :occurred_on, :date

    timestamps()
  end

  @doc false
  def changeset(%Entry{} = entry, attrs) do
    entry
    |> cast(attrs, [:count, :occurred_on, :occurred_at])
    |> validate_required([:count, :occurred_on, :occurred_at])
  end
end
