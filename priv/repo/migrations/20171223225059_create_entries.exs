defmodule Ninox.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :count, :integer
      add :occurred_on, :date
      add :occurred_at, :utc_datetime

      timestamps()
    end

  end
end
