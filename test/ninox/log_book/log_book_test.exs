defmodule Ninox.LogBookTest do
  use Ninox.DataCase

  alias Ninox.LogBook

  describe "entries" do
    alias Ninox.LogBook.Entry

    @valid_attrs %{count: 42, occurred_at: "2010-04-17 14:00:00.000000Z", occurred_on: ~D[2010-04-17]}
    @update_attrs %{count: 43, occurred_at: "2011-05-18 15:01:01.000000Z", occurred_on: ~D[2011-05-18]}
    @invalid_attrs %{count: nil, occurred_at: nil, occurred_on: nil}

    def entry_fixture(attrs \\ %{}) do
      {:ok, entry} =
        attrs
        |> Enum.into(@valid_attrs)
        |> LogBook.create_entry()

      entry
    end

    test "list_entries/0 returns all entries" do
      entry = entry_fixture()
      assert LogBook.list_entries() == [entry]
    end

    test "get_entry!/1 returns the entry with given id" do
      entry = entry_fixture()
      assert LogBook.get_entry!(entry.id) == entry
    end

    test "create_entry/1 with valid data creates a entry" do
      assert {:ok, %Entry{} = entry} = LogBook.create_entry(@valid_attrs)
      assert entry.count == 42
      assert entry.occurred_at == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
      assert entry.occurred_on == ~D[2010-04-17]
    end

    test "create_entry/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = LogBook.create_entry(@invalid_attrs)
    end

    test "update_entry/2 with valid data updates the entry" do
      entry = entry_fixture()
      assert {:ok, entry} = LogBook.update_entry(entry, @update_attrs)
      assert %Entry{} = entry
      assert entry.count == 43
      assert entry.occurred_at == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
      assert entry.occurred_on == ~D[2011-05-18]
    end

    test "update_entry/2 with invalid data returns error changeset" do
      entry = entry_fixture()
      assert {:error, %Ecto.Changeset{}} = LogBook.update_entry(entry, @invalid_attrs)
      assert entry == LogBook.get_entry!(entry.id)
    end

    test "delete_entry/1 deletes the entry" do
      entry = entry_fixture()
      assert {:ok, %Entry{}} = LogBook.delete_entry(entry)
      assert_raise Ecto.NoResultsError, fn -> LogBook.get_entry!(entry.id) end
    end

    test "change_entry/1 returns a entry changeset" do
      entry = entry_fixture()
      assert %Ecto.Changeset{} = LogBook.change_entry(entry)
    end
  end
end
